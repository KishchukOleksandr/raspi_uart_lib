#include <string>
#include <termios.h>
#include <unistd.h>
#include <vector>

#define READ_BUFFER_SIZE 1024

class RPiUart
{
  public:
    enum class BaudRate
    {
     BR_0,
     BR_1200,
     BR_2400,
     BR_4800,
     BR_9600,
     BR_19200,
     BR_38400,
     BR_57600,
     BR_115200,
     BR_230400,
     BR_460800,
     BR_921600,
    };
  private:
    int ufd;
    std::string name;
    BaudRate br;
    char rdBuffer[READ_BUFFER_SIZE];
  public:
    RPiUart(std::string& name, BaudRate br = BaudRate::BR_115200);
    ~RPiUart();
    void Read(std::string& data);
    void ReadBinary(std::vector<uint8_t>& data);
    void Write(const std::string& data);
    void WriteBinary(std::vector<uint8_t>& data);
};
