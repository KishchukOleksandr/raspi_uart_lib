#include "RPiUart.hpp"
#include <iostream>
#include <fcntl.h>

RPiUart::RPiUart(std::string &name, RPiUart::BaudRate br)
    : name{name}, br{br}
{
  const char *c_name = this->name.c_str();
  ufd = open(c_name, O_RDWR | O_NOCTTY);

  if (ufd == -1)
    std::cout << "Error - unable to open " << this->name << std::endl;

  struct termios opt;
  tcgetattr(ufd, &opt);
  opt.c_cflag = CS8 | CLOCAL | CREAD;

  switch (br)
  {
  case BaudRate::BR_0:
    opt.c_cflag |= B0;
    break;
  case BaudRate::BR_1200:
    opt.c_cflag |= B1200;
    break;
  case BaudRate::BR_2400:
    opt.c_cflag |= B2400;
    break;
  case BaudRate::BR_4800:
    opt.c_cflag |= B4800;
    break;
  case BaudRate::BR_9600:
    opt.c_cflag |= B9600;
    break;
  case BaudRate::BR_19200:
    opt.c_cflag |= B19200;
    break;
  case BaudRate::BR_38400:
    opt.c_cflag |= B38400;
    break;
  case BaudRate::BR_57600:
    opt.c_cflag |= B57600;
    break;
  case BaudRate::BR_115200:
    opt.c_cflag |= B115200;
    break;
  case BaudRate::BR_230400:
    opt.c_cflag |= B230400;
    break;
  case BaudRate::BR_460800:
    opt.c_cflag |= B460800;
    break;
  case BaudRate::BR_921600:
    opt.c_cflag |= B921600;
    break;
  }
  opt.c_iflag = IGNPAR;
  opt.c_oflag = 0;
  opt.c_lflag = 0;
  tcflush(ufd, TCIFLUSH);
  tcsetattr(ufd, TCSANOW, &opt);
}

RPiUart::~RPiUart()
{
  int res = close(this->ufd);
  if (res == -1)
    std::cout << "Uart " << this->name << " close error\n";
  else
    std::cout << "Uart " << this->name << " is successfully closed\n";
}

void RPiUart::Read(std::string &data)
{
  ssize_t res = read(this->ufd, this->rdBuffer, READ_BUFFER_SIZE);

  if(res > 0)
    data += std::string(this->rdBuffer, res);
  else
    std::cout << "Read data error!!!\n";
}

void RPiUart::Write(const std::string &data)
{
  ssize_t res = write(this->ufd, data.c_str(), data.size());

  if (res == -1)
    std::cerr << "RPiUart error write data!!!\n";
}
