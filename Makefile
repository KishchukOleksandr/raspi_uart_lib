hello_uart: hello_uart.o RPiUart.o
	g++ -o hello_uart hello_uart.o RPiUart.o

hello_uart.o: hello_uart.cpp RPiUart.hpp
	g++ -c hello_uart.cpp

RPiUart.o: RPiUart.cpp RPiUart.hpp
	g++ -c RPiUart.cpp

.PHONY: clean
clean:
	rm *.o hello_uart